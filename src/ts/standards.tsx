import type { ProColumns } from '@ant-design/pro-components';
import { EditableProTable } from '@ant-design/pro-components';
import { Col, Input, Row, Modal } from 'antd';
import React, { useState, FC } from 'react';
import { getStandardsAsync, addStandardsAsync, delStandardsAsync, updateStandardsAsync } from '../api/standards'
import './standards.css';

type DataSourceType = {
  id: React.Key;
  standard?: any;
  part?: any;
  category?: any;
  created_at?: string;
  parameters?:string,
  children?: DataSourceType[];
  isAdd?: boolean;
};

const getStandards = async () => {
	var res = await getStandardsAsync();
	var data = res.data
	var list = [];
	for(var i=0; i<data.length; i++){
		var row = {
			id: data[i].id,
			standard: data[i].attributes.standard,
			part: data[i].attributes.part,
			parameters: data[i].attributes.parameters == null ? "" : JSON.stringify(data[i].attributes.parameters),
			category: data[i].attributes.category
		};
		list.push(row)
	}
	var obj={
		data: list
	}
	return obj;
}

interface LooseObject {
    [key: string]: any
}

var inputObj:LooseObject = {};
var standardObj:DataSourceType;
//在输入框发生变化的时候修改状态的值
const inputChange =(key:string, val:string, isKey:boolean)=>{
	if(isKey){
		inputObj[key+"_key"]=val;
	}else{
		inputObj[key+"_val"]=val;
	}
	console.log(key)
}

const MyTable: FC = () => {
  const [visible, setVisible] = useState(false);
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>([]);
  const [dataSource, setDataSource] = useState<DataSourceType[]>([]);
  const [position] = useState<'top' | 'bottom' | 'hidden'>('bottom');
  const [modalContent, setModalContent] = useState<any[]>([])

  const columns: ProColumns<DataSourceType>[] = [
    {
      title: 'STANDARD',
      dataIndex: 'standard',
	  width: 100,
	  sorter: (a, b) => a.standard.localeCompare(b.standard),
	  valueType: 'select',
	  valueEnum: {
	    metric: {
			text: 'metric',
			status: 'metric',
	    },
	    imperial: {
	    	text: 'imperial',
	    	status: 'imperial',
	    },
		US: {
			text: 'US',
			status: 'US',
		},
		hasco: {
			text: 'hasco',
			status: 'hasco',
		},
		lkm: {
			text: 'lkm',
			status: 'lkm',
		},
	  },
    },
    {
      title: 'PART',
      dataIndex: 'part',
	  width: 100,
	  // sorter: (a, b) => a.part.localeCompare(b.part),
    },
    {
      title: 'CATEGORY',
      dataIndex: 'category',
	  width: 100,
	  // sorter: (a, b) => a.category.localeCompare(b.category),s
	},
	{
	  title: 'PARAMETERS',
	  dataIndex: 'parameters',
	  width: 200
	},
    {
      title: '操作',
      valueType: 'option',
      width: 200,
      render: (text, record, _, action) => [
        <a
		  href="/#"
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
        >
          编辑
        </a>,
        <a
		  href="/#"
          key="delete"
          onClick={ async() => {
			  var res = await delStandardsAsync(record);
			  if(res && res.data.id === record.id)
				setDataSource(dataSource.filter((item) => item.id !== record.id));
			
          }}
        >
          删除
        </a>,
		<a
		  href="/#"
		  key="param"
		  onClick={ async() => {
			  setVisible(true);
			  standardObj=record;
			  let content:any = [];
			  if(record.parameters && record.parameters !== ""){
				  let obj = JSON.parse(record.parameters);
				  Object.keys(obj).map(attr => {
					  let index = (Math.random() * 1000000).toFixed(0);
					  let row = (
						<Input.Group size="small" key={index}>
						  <Row gutter={8}>
							<Col span={6}>
							  <Input placeholder="Key" onChange={(e) => { inputChange(index, e.target.value, true)}} defaultValue={attr} />
							</Col>
							  <span>=</span>
							<Col span={12}>
								<Input placeholder="Value(多个，以逗号隔开)" onChange={(e) => { inputChange(index, e.target.value, false)}} defaultValue={obj[attr]} />
							</Col>
							<Col span={3}>
								<a id={index} href="/#" key="param" onClick={ async() => {
									let parametersObj = obj;
									delete parametersObj[attr];
									standardObj.parameters=JSON.stringify(parametersObj);
									setModalContent(modalContent => modalContent.filter(item => item.key !== index));
								}}>删除</a>
							</Col>
						  </Row>
						  <p/>
						</Input.Group>);
					  content.push(row);
				  });
			   }
			   setModalContent(content);
		  }}
		>
		  编辑参数
		</a>,
      ],
    },
  ];

  return (
    <>
      <EditableProTable<DataSourceType>
        rowKey="id"
        headerTitle=""
        scroll={{
          x: 960,
        }}
        recordCreatorProps={
          position !== 'hidden'
            ? {
                position: position as 'top',
                record: () => ({ 
					id: (Math.random() * 1000000).toFixed(0),
					isAdd: true
				}),
              }
            : false
        }
        loading={false}
        toolBarRender={() => [
          // <Button>查询</Button>
        ]}
        columns={columns}
        request={getStandards}
        value={dataSource}
        onChange={setDataSource}
        editable={{
          type: 'multiple',
          editableKeys,
          onSave: async (rowKey, data, row) => {
			// console.log(JSON.stringify(param));
			if(data.isAdd){
				//新增
				var res = await addStandardsAsync(data);
				if(res){
					delete data.isAdd
				}
				
			}else{
				//编辑
				await updateStandardsAsync(data);
			}
            // await waitTime(2000);
          },
          onChange: setEditableRowKeys,
        }}
      />
		
	  <Modal
		  title="Params Editor"
		  centered
		  visible={visible}
		  okText="保存"
		  onOk={ async() => {
			  let inputParams = standardObj['parameters'] ? JSON.parse(standardObj['parameters']) : {};
			  Object.keys(inputObj).map(k => {
			      if(k.indexOf('_key') !== -1){
			      	var _k = k.split("_")[0];
					var key = inputObj[_k+"_key"];
					var value = inputObj[_k+"_val"];
					if(value.indexOf(",") !== -1){
						value = value.split(",");
						for(let i=0; i<value.length; i++){
							value[i] = Number(value[i]);
						}
					}else
						value = Number(value);
			      	inputParams[key] = value;
					
			      }
			  })
			  standardObj['parameters'] = JSON.stringify(inputParams);
			  //编辑
			  let res = await updateStandardsAsync(standardObj);
			  if(res){
				  alert("保存成功")
			  }
		  }}
		  onCancel={() => setVisible(false)}
		  width={500}
		  bodyStyle={{
			  minHeight: 200,
		  }}
		>
		  {modalContent}
		  <a href="/#" key="param" onClick={ async() => {
			  let content:any = modalContent.slice();
			  let index = (Math.random() * 1000000).toFixed(0);
			  let row = (<Input.Group size="small" key={index}>
						  <Row gutter={8}>
							<Col span={6}>
							  <Input placeholder="Key" onChange={(e) => { inputChange(index, e.target.value, true)}} />
							</Col>
							  <span>=</span>
							<Col span={12}>
								<Input placeholder="Value(多个，以逗号隔开)" onChange={(e) => { inputChange(index, e.target.value, false)}} />
							</Col>
							<Col span={3}>
								<a id={index} href="/#" key="param" onClick={ async() => {
									setModalContent(modalContent => modalContent.filter(item => item.key !== index));
								}}>删除</a>
							</Col>
						  </Row>
						  <p/>
						</Input.Group>);
			  content.push(row);
			  setModalContent(content);

		  }}>添加一行</a>
		  
		</Modal>
    </>
  );
};

export default MyTable;