import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:1344';
// axios.defaults.baseURL = 'http://192.168.0.103:1344';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/json';

// 响应拦截器
axios.interceptors.response.use(
    res => res.data,  // 拦截到响应对象，将响应对象的 data 属性返回给调用的地方
    err => Promise.reject(err)
)