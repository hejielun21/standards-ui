import axios from 'axios'

export const getStandardsAsync = () => axios.get('/api/standards')

export const addStandardsAsync = param => axios.post('/api/standards', {
	data : param
});

export const delStandardsAsync = param => axios.delete('/api/standards/' + param.id);

export const updateStandardsAsync = (param) => axios.put('/api/standards/' + param.id, {
	data : param
});